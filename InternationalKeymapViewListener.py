import sublime
import sublime_plugin

from .InternationalKeymap import InternationalKeymap

class InternationalKeymapViewListener(sublime_plugin.ViewEventListener):

	def on_post_save_async(self):
		file_name = self.view.file_name()
		if (
			file_name.endswith('.sublime-keymap')
			and file_name != InternationalKeymap.get().path
		):
			InternationalKeymap.get().try_generate()
