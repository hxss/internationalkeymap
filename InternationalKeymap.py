import sublime
import sublime_plugin

from .lib.Singleton import Singleton
from .lib.SublimePluginSettings import SublimePluginSettings

import os
import re
import hashlib

class InternationalKeymap(SublimePluginSettings, metaclass=Singleton):

	def __init__(self):
		self.settings.clear_on_change('layouts')
		self.settings.add_on_change(
			'layouts',
			self.settings_update
		)
		self.settings_update()

	def settings_update(self):
		super().settings_update()

		if (hasattr(self, '_layouts')):
			del(self._layouts)

		if (hasattr(self, '_synonyms')):
			del(self._synonyms)

	def generate(self):
		inter_keymap = []
		for km, keymap in self.keymaps.items():
			for s, shortcut in enumerate(keymap['keys']):
				for k, key in enumerate(shortcut):
					for synonym in self.synonyms.get(key, []):
						keymap['keys'][s][k] = synonym
						inter_keymap.append(keymap)

		for km, keymap in enumerate(inter_keymap):
			for s, shortcut in enumerate(keymap['keys']):
				inter_keymap[km]['keys'][s] = self.join_shortcut(shortcut)

		self.save(inter_keymap)

	def save(self, keymap):
		if (not os.path.isdir(self.path_plugin)):
			os.makedirs(self.path_plugin)

		with open(self.path, 'w') as f:
			f.write(sublime.encode_value(keymap, True))

	@property
	def path_plugin(self):
		return (
			sublime.packages_path()
			+ '/' + self.__class__.__name__
		)

	@property
	def path(self):
		return (
			self.path_plugin + '/'
			+ 'Default.sublime-keymap'
		)

	@property
	def qwerty(self):
		return self.split_layout(
			self.settings.get('layouts')['qwerty']
		)

	@property
	def layouts(self):
		if (not hasattr(self, '_layouts')):
			self._layouts = self.settings.get('layouts')
			del(self._layouts['qwerty'])

			for k in self._layouts:
				self._layouts[k] = self.split_layout(self._layouts[k])

		return self._layouts

	@property
	def synonyms(self):
		if (not hasattr(self, '_synonyms')):
			self._synonyms = {}
			for i, key in enumerate(self.qwerty):
				for l in self.layouts:
					if (self.layouts[l][i] != key):
						self._synonyms[key] = self._synonyms.get(key, [])
						self._synonyms[key].append(self.layouts[l][i])

		return self._synonyms

	def split_layout(self, layout):
		return [m[0] + m[1] for m in re.findall(r'(?:\((.+?)\)|(\S))', layout)]

	@property
	def keymaps(self):
		keymap = {}
		for km in self.get_keymap_resources():
			rules = sublime.decode_value(sublime.load_resource(km))
			for rule in rules:
				keys = '_'.join(rule['keys']) + '_%s' % id(rule.get('context', None))
				is_composite_keymap = False
				for i, shortcut in enumerate(rule['keys']):
					rule['keys'][i] = self.split_shortcut(shortcut)
					is_composite_keymap = is_composite_keymap or (len(rule['keys'][i]) > 1)

				if (
					self.settings.get('single_keys', False)
					or is_composite_keymap
				):
					keymap[keys] = rule

		return keymap

	def get_keymap_resources(self):
		platform = {
			'osx': 'OSX',
			'linux': 'Linux',
			'windows': 'Windows',
		}.get(sublime.platform())

		return [
			km
			for km in sublime.find_resources('*.sublime-keymap')
			if re.match('.*Default( \(' + platform + '\))?\.sublime-keymap', km)
		]

	def split_shortcut(self, shortcut):
		return re.compile(r'(?:(?<!\+)|(?<=\+\+))\+').split(shortcut)

	def join_shortcut(self, shortcut):
		return (
			'+'.join(shortcut)
			if isinstance(shortcut, list)
			else shortcut
		)

	def get_hash(self):
		sha1 = hashlib.sha1()
		for km in self.get_keymap_resources():
			if (not self.path.endswith(km)):
				sha1.update(sublime.load_binary_resource(km))

		return sha1.hexdigest()

	@property
	def path_temp(self):
		return self.path_plugin + '/temp'

	@property
	def path_hash(self):
		return self.path_temp + '/keymaps.sha1'

	def try_generate(self):
		if (
			self.settings.get('auto_generate', False)
			and self.is_keymaps_modified()
		):
			self.generate()
			self.save_hash()

	def save_hash(self):
		if (not os.path.isdir(self.path_temp)):
			os.makedirs(self.path_temp)

		with open(self.path_hash, 'w') as fsha1:
			return fsha1.write(self.get_hash())

	def is_keymaps_modified(self):
		if (os.path.isdir(self.path_temp)):
			with open(self.path_hash, 'r') as fsha1:
				return fsha1.read() != self.get_hash()

		return True
