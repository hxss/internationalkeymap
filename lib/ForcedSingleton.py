
from .Singleton import Singleton

class ForcedSingleton(Singleton):
	_instances = {}

	def __call__(cls, *args, **kwargs):
		return Singleton.get(cls, *args, **kwargs)
