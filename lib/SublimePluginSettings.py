import sublime

class SublimePluginSettings:

	def get_settings_name(self):
		return self.__class__.__name__ + '.sublime-settings'

	def settings_update(self):
		self._settings = sublime \
			.load_settings(self.get_settings_name())

	@property
	def settings(self):
		if (not hasattr(self, '_settings')):
			self.settings_update()

		return self._settings
