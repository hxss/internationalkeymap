import sublime
import sublime_plugin

import threading

from .SublimePluginSettings import SublimePluginSettings

class SublimePluginSettingsEditor(SublimePluginSettings):

	def get_settings_name(self):
		return 'Preferences.sublime-settings'

	@property
	def plugin_name(self):
		return ''

	@property
	def template(self):
		return '{\n' + self.separator + '\n}'

	@property
	def template_point(self):
		line = 1
		row = (
			self.settings.get('tab_size', 4)
			if (self.settings.get('translate_tabs_to_spaces', False))
			else 1
		)

		return (line, row)

	@property
	def separator(self):
		return (
			' ' * self.settings.get('tab_size', 4)
			if (self.settings.get('translate_tabs_to_spaces', False))
			else '\t'
		)

	@property
	def file_plugin(self):
		return (
			sublime.packages_path() + '/'
				+ self.plugin_name + '/'
				+ self.plugin_name + '.sublime-settings'
		)

	@property
	def file_user(self):
		return (
			sublime.packages_path() + '/'
				+ 'User/'
				+ self.plugin_name + '.sublime-settings'
		)


	def open(self):
		sublime.run_command('new_window')
		window = sublime.active_window()

		window.open_file(self.file_plugin)
		view = window.open_file(self.file_user)

		window.run_command('new_pane')
		window.set_view_index(
			view,
			1,
			0
		)

		threading.Thread(target=self.paste_template, args=(view,)).start()

	def paste_template(self, view):
		while (view.is_loading()):
			pass

		if (view.size() == 0):
			view.run_command("append", {"characters": self.template})

			pt = view.text_point(self.template_point[0], self.template_point[1])
			view.sel().clear()
			view.sel().add(sublime.Region(pt))


