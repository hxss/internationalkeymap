import sublime
import sublime_plugin

from .InternationalKeymap import InternationalKeymap

class InternationalKeymapGenerate(sublime_plugin.ApplicationCommand):

	def run(self):
		InternationalKeymap.get().generate()

def plugin_loaded():
	InternationalKeymap.get().try_generate()
