# InternationalKeymap
Creates localised keymaps based on sublime keymaps files.

As you can see sublime keymaps created for English layout doesn't works when you change keyboard layout on your local. More precisely its doesn't work for different characters on same keys. This problem especialy perceptible for Cyrillic layouts.

This plugin helps you to translate existing English keymaps to your local layout.

## IMPORTANT
Because of python doesn't provide access to system layouts without root privilege you must describe your layout by yourself in the settings in the specified format.

## Settings

###Layouts
The main settings element is `layouts` object that contain `qwerty` layout and infinite local layouts. The `qwerty` layout is reference layout that will be compared with any other for translating.

The format of layouts is very simple. You just write all needed for translate symbols in English(`qwerty`) layout and than you type same keys with local symbols in your local layout in same order. During the translating all symbols in local layouts will be compared with symbols at same positions in `qwerty` and if its differs - will be created new translated keymap for this symbol.

E.g. by default the plugin settings contain `qwerty` and `йцукен`(Cyrillic) layouts. This is just 4 rows of main qwerty keyboard keys typed ltr - ``123...bnm,./`. But you don't have to describe all keys or use this order. For your case you can describe any count of keys in any order. The main thing is that they must follow in the same order for every layout.

You can use verbal symbol names in your layouts. This names must be in brackets. All space characters in layouts will be ignored.

E.g.:
```
{
	"layouts": {
		"qwerty": "`(minus)[",
		"йцукен": "ёг      х"
	}
}
```
In this example grave symbol will be translated as Cyrillic 'ё', all occurrences of `minus` will be translated as 'г', '[' >> 'х'(Cyrillic).

###Auto_generate [true|false]
Second option - `auto_generate` allow to controll automatic generation of local keymaps on sublime loading or when update(save) any `*.sublime-keymap` file. Auto generation first verify checksums of `*.sublime-keymap` files and only if its was modified generate new keymap rules.

###Single_keys [true|false]
Last option is `single_keys` - controls translation of keymaps consisting from 1 key. As `[` that using for insert snippets.

##Using

###Auto generation

As you already know the plugin can automatically generate localised keymaps if appropriate option enabled in settings.

###Manual generate command

In sublime [comand palette](http://docs.sublimetext.info/en/latest/reference/command_palette.html) you can use command `International keymap: Generate` for manual generation of keymaps.

###Settings editing

Another command `Preferences: International keymaps` helps you to open the plugin settings files.
