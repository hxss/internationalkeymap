import sublime
import sublime_plugin

from .lib.SublimePluginSettingsEditor import SublimePluginSettingsEditor

class InternationalKeymapPreferences(sublime_plugin.ApplicationCommand, SublimePluginSettingsEditor):

	def run(self):
		self.open()

	@property
	def plugin_name(self):
		return 'InternationalKeymap'
